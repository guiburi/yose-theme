# YoseTheGame - ThemeTeam

clone
`git clone git@bitbucket.org:guiburi/yose-theme.git`

Run this cmd to copy .env.example into .env
`cp .env.example .env`

run `php artisan --no-ansi key:generate`

then `composer install`

for built in server `php artisan serve`

for adding ssh key to bitbucket
https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html
